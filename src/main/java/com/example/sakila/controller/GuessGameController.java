package com.example.sakila.controller;

import com.example.sakila.domain.GuessGame;
import com.example.sakila.domain.GuessGameRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@RestController
public class GuessGameController {
    @Autowired
    GuessGameRepo guessGameRepo;

    @GetMapping("/guess-game/start")
    public GuessGame start() {
        Long no = Double.valueOf(Math.random() * 100).longValue();
        GuessGame guessGame = new GuessGame();
        guessGame.setAnswer(no);
        guessGameRepo.save(guessGame);
        // save this num into guess_game table
        // return the guess_game.id
//        Map<String, Integer> map = new HashMap<>();
//        map.put("id", no);
//        return map;
        return guessGame;
    }

    @GetMapping("/guess-game/guess/{num}/{id}")
    public Map guess(@PathVariable int num, @PathVariable Long id) {
        // read the no from guess_game table, then compare
        Optional<GuessGame> opt = guessGameRepo.findById(id);
        String message = "";
        String status = "ko";
        if (opt.isPresent()) {
            GuessGame guessGame = opt.get();
            if (guessGame.getAnswer() == num) {
                message = "You win";
                status = "ok";
            } else if (guessGame.getAnswer() > num) {
                message = "Guess Higher";
            } else {
                message = "Guess Lower";
            }
        } else {
            message = "not valid id";
            status = "err";
        }
        Map<String, String> map = new HashMap<>();
        map.put("status", status);
        map.put("message", message);
        return map;
    }

    @GetMapping("/test")
    public ResponseEntity<?> test() {
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return new ResponseEntity<String>("{\"result\":\"ok\", \"message\":\"testing\"}", httpHeaders, HttpStatus.OK);
    }

    @GetMapping("/test2")
    public int test2() {
        return 10;
    }
}
